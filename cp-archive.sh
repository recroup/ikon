#!/bin/sh

if [ -z "$1" ]
  then
    echo "Must pass icomoon archive's path as an argument"
    exit
fi

TEMP_PATH="/tmp/ikon-data/"

if ! command -v woff2_compress &> /dev/null
then
  git clone --recursive https://github.com/google/woff2.git $TEMP_PATH'woff2'
  CURRENT_PATH=$(pwd)
  cd ${TEMP_PATH}'woff2'
  make clean all
  sudo mv woff2_compress woff2_decompress woff2_info /usr/local/bin/
  cd $CURRENT_PATH
fi

unzip -d $TEMP_PATH $1

STYLE=${TEMP_PATH}'style.scss'

woff2_compress ${TEMP_PATH}'fonts/fa.ttf'

TOKEN=$(sed -rn "s/.*woff\?([[:alnum:]]+)'.*/\1/p" $STYLE)

echo $TOKEN

sed -i '' '6i\'$'\n'"    url('#{\$icomoon-font-path}/#{\$icomoon-font-family}.woff2?$TOKEN') format('woff2'),
" $STYLE

sed -i '' '4i\'$'\n'"    url('fonts/fa.woff2?$TOKEN') format('woff2'),
" ${TEMP_PATH}'style.css'

mv ${TEMP_PATH}demo.html ${TEMP_PATH}index.html

cp -Rf ${TEMP_PATH}demo-files ${TEMP_PATH}fonts ${TEMP_PATH}index.html ${TEMP_PATH}selection.json ${TEMP_PATH}style.css ${TEMP_PATH}style.scss ${TEMP_PATH}variables.scss ./

rm -rf $TEMP_PATH

