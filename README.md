# README #

A custom list of fonts used in "Optimize".

### What is this repository for? ###

"Optimize" only uses a fraction of icons that libraries such as Font Awesome provides. The Aim of this project is to only keep icons that are necessary in order to reduce to asset size. Utilize icons from other font libraries without cost of including an entire library to use few icons.

This project is built using tool [Icomoon](https://icomoon.io).
### How do I get set up? ###


### Manual ###

In order to customize fonts, follow the steps below

- Install the [Icomoon chrome extension](https://chrome.google.com/webstore/detail/icomoon/kppingdhhalimbaehfmhldppemnmlcjd?hl=en) or visit [Icomoon Online App](https://icomoon.io/app/#/select)
- If this is your first time, import existing icons by clicking on the "Import Icons" buttons on the top left corner.
- Upload the `selection.json` file found in this project, and click `yes` to import all the settings.
- This project has majority of its fonts extracted from Font Awesome, so I would recommend to add this along with many other fonts available under Icomoon library.
- In order to import other icon libraries, click on the hamburger button on the top left corner and then click on the icon library.
- Icomoon does not generate `woff2` fonts in the free version, there are many online tools that can generate `woff2` using the `woff` file, but I use this [website](https://everythingfonts.com/woff-to-woff2).
- Once you have generated new fonts replace the list of files and folders
    - fonts
    - style.css
    - index.html(generated as demo.html)
    - style.scss
    - variables.scss
    - selection.json
- Don't forget to add the woff2 font to the `fonts` folder and related code to `style.scss` and `style.css`.
    - Ensure that font src URLs are correctly ordered (woff2 -> woff -> ttf -> svg)
- Ensure that ligatures are turned off before downloading the folder from Icomoon (button on the top menu of the `Font` section of the app/extension)

### Automated ###

- Adding fonts is pretty boring and a big waste of time, so `cp-archive.sh` was created. It will take care of all the steps mentioned above. It's tested on macos but it should work on any unix system. Once you have the generated archive file from icomoon run this script by passing the path of the archive as an argument. Finally test the code to make sure the changes are as expected.
```
    ./cp-archive.sh <absolute_path_of_archive>
```
Make sure to check that the `cp-archive.sh` is executeable. If its not run this command to make it executable
```
    chmod +x cp-archive.sh
```

Finally make sure to upgrade the version in the `package.json` file.



